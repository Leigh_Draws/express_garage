require('dotenv').config();
const cors = require('cors');
const express = require('express');
const app = express();

app.use(cors({
    origin: 'http://localhost:8080'
}));
app.use(express.json());

app.use(
    express.urlencoded({
        extended: true,
    })
);

// Importe les middleware et les controller
const {logRequest} = require ('./middleware/logger.middleware')
const carsController = require('./controllers/cars.controller')
const garageController = require('./controllers/garage.controller')

// Utilisation du middleware

app.use(logRequest)

// Routes du CRUD

app.get('/', (req, res) => {
    res.send('HELLO')
});
app.use('/cars', carsController);
app.use('/garages', garageController );

const port = 3000;
app.listen(port, () => {
    console.log(`Serveur en écoute sur le port ${port}`);
});
