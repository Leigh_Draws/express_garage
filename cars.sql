CREATE DATABASE express_garage;

USE express_garage;

CREATE USER 'garadm7841'@'localhost' IDENTIFIED BY 'SP7c3$@uwL84jmSEoP3';
GRANT ALL PRIVILEGES ON express_garage.* TO 'garadm7841'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE car (
car_id INT PRIMARY KEY AUTO_INCREMENT,
brand VARCHAR(255) NOT NULL,
model VARCHAR(255) NOT NULL
);

CREATE TABLE garage (
garage_id INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255) NOT NULL,
email VARCHAR(255) NOT NULL
);

INSERT INTO car (brand, model)
VALUES 
("Audi", "A1"),
("Citroën", "C4"),
("Toyota", "Verso"),
("Renault", "Megane");

INSERT INTO garage (name, email)
VALUES
("La voiture à Titi", "titicarauto@gmail.com"),
("Sur les chapeaux de roues", "Chapodrou@mail.fr"),
("Meilleur garage", "presquelemeilleur@yahoo.fr");