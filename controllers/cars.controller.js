const express = require('express');
const router = express.Router();

// Connexion à la base de données 
const connection = require("../conf/db");

// Routes en GET 

// Afficher toutes les voitures
router.get('/', (req, res) => {
    connection.query('SELECT * FROM car', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des voitures');
        } else {
            res.json(results)
        }
    })
});

// Afficher une voiture grâce à sa marque
router.get('/brand/:brand', (req, res) => {
    const brand = req.params.brand;
    connection.query('SELECT * FROM car WHERE brand = ?', [brand], (err, results) => {
        if (err) {
            res.status(500).send(`Erreur lors de la récupération des voitures de la marque ${brand}`);
        } else {
            res.json(results)
        }
    })
});

// Afficher une voiture grâce à son ID
router.get('/:id', (req, res) => {
    const id = req.params.id;
    connection.execute('SELECT * FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la voiture');
        } else {
            res.json(results);
        }
    })
});

// Routes en POST

// Ajouter une voiture
router.post('/', (req, res) => {
    const { brand, model } = req.body;
    connection.execute('INSERT INTO car (brand, model) VALUES (?, ?)', [brand, model], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de l\'ajout de la voiture');
        } else {
            res.status(201).send(`Voiture ajoutée avec l'ID ${results.insertId}`);
        }
    })
});


// Routes en PUT

// Modifier une voiture grâce à son ID
router.put('/:id', (req, res) => {
    const { brand, model } = req.body;
    const id = req.params.id;
    connection.execute('UPDATE car SET brand = ?, model = ? WHERE car_id = ?', [brand, model, id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la modification de la voiture');
        } else {
            res.status(201).send(`Voiture mise à jour`);
        }
    })
});

// Routes en DELETE

// Supprimer une voiture
router.delete('/:id', (req, res) => {
    const id = req.params.id;
    connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la supression de la voiture');
        } else {
            res.send('Voiture supprimée');
        }
    })
});


module.exports = router;