const express = require('express');
const router = express.Router();
const { validateGarageData } = require('../middleware/validator.middleware');

// Connexion à la base de données 
const connection = require("../conf/db");

// Routes en GET 

// Afficher tous les garages
router.get('/', (req, res) => {
    connection.query('SELECT * FROM garage', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des garages');
        } else {
            res.json(results)
        }
    })
});

// Afficher un garage grâce à son ID
router.get('/:id', (req, res) => {
    const id = req.params.id;
    connection.execute('SELECT * FROM garage WHERE garage_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération du garage');
        } else {
            res.json(results);
        }
    })
});

// Routes en POST

// Ajouter un garage
router.post('/', validateGarageData, (req, res) => {
    const { name, email } = req.body;
    connection.execute('INSERT INTO garage (name, email) VALUES (?, ?)', [name, email], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de l\'ajout du garage');
        } else {
            res.status(201).send(`Garage ajouté avec l'ID ${results.insertId}`);
        }
    })
});


// Routes en PUT

// Modifier un garage grâce à son ID
router.put('/:id', validateGarageData, (req, res) => {
    const { name, email } = req.body;
    const id = req.params.id;
    connection.execute('UPDATE garage SET name = ?, email = ? WHERE garage_id = ?', [name, email, id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la modification du garage');
        } else {
            res.status(201).send(`Garage mis à jour`);
        }
    })
});

// Routes en DELETE

// Supprimer un garage
router.delete('/:id', (req, res) => {
    const id = req.params.id;
    connection.execute('DELETE FROM garage WHERE garage_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la supression du garage');
        } else {
            res.send('Garage supprimé');
        }
    })
});


module.exports = router;