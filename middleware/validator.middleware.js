const validateGarageData = (req, res, next) => {
    const { name, email } = req.body;
  
    // Vérifier que le titre et l'auteur sont présents dans la requête
    if (!name || !email) {
      return res.status(400).json({ message: 'L\'email et le nom sont obligatoires.' });
    }
  
    // Validation de l'email
    let regex =  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if(!email.match(regex)){
        return res.status(400).json({ message: 'Ce n\'est pas une adresse mail.'})
    }

    // passe au middleware suivant
    next();
  };
  
  module.exports = { validateGarageData };
  